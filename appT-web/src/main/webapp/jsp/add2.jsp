<%@page import="java.util.List"%>
<%@page import="com.afpa.cda.dto.MetierDto"%>
<%@page import="com.afpa.cda.service.IMetierService"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<title>Ajouter un métier</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Ajouter un métier</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav">
				<a class="nav-item nav-link active" href="index.html"><i
					class="fas fa-home"></i> Accueil <span class="sr-only">(current)</span></a>
				<a class="nav-item nav-link" href="add.do"><i
					class="fas fa-plus"></i> Ajouter une personne</a>
			</div>
		</div>
	</nav>

	<div class="container my-4">
		<div class="row justify-content-md-center">
			<div class="col card col-lg-12">
				<form action="add2.do" method="post">
					<div class="form-group">
						<label for="label">Label :</label> 
						<input type="text" class="form-control" name="label" id="label" required placeholder="Développeur">
					</div>
					<button type="submit" class="btn btn-primary btn-dark">Ajouter
						le métier !</button>
				</form>
				<a class="btn btn-secondary btn-dark" href="list.do" role="button">retour
					vers la liste</a>
			</div>
		</div>
	</div>
	<script src="jquery/jquery-3.3.1.slim.min.js"></script>
	<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>