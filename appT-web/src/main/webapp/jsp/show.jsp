<%@page import="com.afpa.cda.dto.MetierDto"%>
<%@page import="java.util.Date"%>
<%@page import="com.afpa.cda.dto.ReponseStatut"%>
<%@page import="com.afpa.cda.tools.Utils"%>
<%@page import="com.afpa.cda.dto.PersonneDto"%>
<%@page import="java.util.Optional"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link href="fontawesome/css/all.min.css" rel="stylesheet">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<title>Show</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Informations personnelles</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav">
				<a class="nav-item nav-link active" href="index.html"><i
					class="fas fa-home"></i> Accueil <span class="sr-only">(current)</span></a>
				<a class="nav-item nav-link" href="add.do"><i
					class="fas fa-plus"></i> Ajouter un utilisateur</a> <a
					class="nav-item nav-link" href="add2.do"><i class="fas fa-plus"></i>
					Ajouter un métier</a>
			</div>
		</div>
	</nav>
	<%
		int id = (int) request.getAttribute("id");
		Optional<PersonneDto> boitePersonne = (Optional<PersonneDto>) request.getAttribute("personne");
		if (boitePersonne.isPresent()) {
			PersonneDto personne = boitePersonne.get();
			Date date = personne.getDateNaissance();
			String[] tabStr = new String[1];
			String str = date == null ? "" : date.toString();
			tabStr = str.split("\\s");
	%>
	<div class="container my-4">
		<div class="row justify-content-md-center">
			<div class="col card col-lg-12">
				<form>
					<div class="form-group row">
						<label for="idPers" class="col-sm-2 col-form-label">Identifiant</label>
						<div class="col-sm-10">
							<input type="text" readonly class="form-control-plaintext"
								id="idPers" value="<%=boitePersonne.get().getId()%>">
						</div>
					</div>
					<div class="form-group row">
						<label for="nomPers" class="col-sm-2 col-form-label">Nom</label>
						<div class="col-sm-10">
							<input type="text" readonly class="form-control-plaintext"
								id="nomPers"
								value="<%=boitePersonne.get().getNom() == null ? "" : boitePersonne.get().getNom()%>">
						</div>
					</div>
					<div class="form-group row">
						<label for="prenomPers" class="col-sm-2 col-form-label">Prénom</label>
						<div class="col-sm-10">
							<input type="text" readonly class="form-control-plaintext"
								id="prenomPers"
								value="<%=boitePersonne.get().getPrenom() == null ? "" : boitePersonne.get().getPrenom()%>">
						</div>
					</div>
					<div class="form-group row">
						<label for="adressePers" class="col-sm-2 col-form-label">Adresse</label>
						<div class="col-sm-10">
							<input type="text" readonly class="form-control-plaintext"
								id="adressePers"
								value="<%=boitePersonne.get().getAdresse() == null ? "" : boitePersonne.get().getAdresse()%>">
						</div>
					</div>
					<div class="form-group row">
						<label for="dateNaissancePers" class="col-sm-2 col-form-label">Date
							de naissance</label>
						<div class="col-sm-10">
							<input type="text" readonly class="form-control-plaintext"
								id="dateNaissancePers" value="<%=tabStr[0]%>">
						</div>
					</div>
					<div class="form-group row">
						<label for="metierPers" class="col-sm-2 col-form-label">Métier</label>
						<div class="col-sm-10">
							<input type="text" readonly class="form-control-plaintext"
								id="metierPers"
								value="<%=boitePersonne.get().getMetier() == null ? "" : boitePersonne.get().getMetier().getLabel() %>">.
								
						</div>
					</div>
				</form>
				<a class="btn btn-secondary btn-dark" href="list.do" role="button">retour
					vers la liste</a>
			</div>
		</div>
	</div>
	<%
		} else {
			Utils.redirect(request, response, getServletContext(), "/list.do", ReponseStatut.KO,
					"Aaucune personne ne possède l'id :" + id);
			return;
		}
	%>
	<script src="jquery/jquery-3.3.1.slim.min.js"></script>
	<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>