package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.PersonneRepository;
import com.afpa.cda.dto.MetierDto;
import com.afpa.cda.dto.PersonneDto;
import com.afpa.cda.entity.Metier;
import com.afpa.cda.entity.Personne;

@Service
public class PersonneServiceImpl implements IPersonneService {

	@Autowired
	private PersonneRepository personneRepository;
	public static final int INC=5;
	public static int fin ; 
	public static int nbrPage;
	
	@Override
	public List<PersonneDto> chercherToutesLesPersonnes(int page) {
		Pageable pageable = PageRequest.of(page, INC);
		nbrPage = this.personneRepository.findAll(pageable).getTotalPages();
		List<PersonneDto> maliste = this.personneRepository.findAll(pageable)
				.stream()
				.map(e->PersonneDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.prenom(e.getPrenom())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}

	@Override
	public boolean deleteById(int id) {
		if (this.personneRepository.existsById(id)) {
			this.personneRepository.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public Optional<PersonneDto> findById(int id) {
		Optional<Personne> pers = this.personneRepository.findById(id);
		Optional<PersonneDto> res = Optional.empty();
		if (pers.isPresent()) {
			Personne p = pers.get();
			PersonneDto persDto = PersonneDto.builder().id(p.getId()).nom(p.getNom()).prenom(p.getPrenom()).dateNaissance(p.getDateNaissance()).adresse(p.getAdresse()).build();
			if(p.getMetier() != null) {
				MetierDto metDto = MetierDto.builder().id(p.getMetier().getId()).label(p.getMetier().getLabel()).build();
				persDto.setMetier(metDto);
			}
			res = Optional.of(persDto);
		}
		return res;
	}

	public Integer ajouterUnePersonne(PersonneDto pDto) {
		Metier m = Metier.builder().label(pDto.getMetier().getLabel()).build();
		Personne p =  Personne.builder().nom(pDto.getNom()).prenom(pDto.getPrenom()).adresse(pDto.getAdresse()).dateNaissance(pDto.getDateNaissance()).metier(m).build();
		p = this.personneRepository.save(p);
		return p.getId();
	}

}
