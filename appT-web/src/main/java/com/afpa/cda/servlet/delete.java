package com.afpa.cda.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.cda.dto.ReponseDto;
import com.afpa.cda.dto.ReponseStatut;
import com.afpa.cda.service.IPersonneService;

@WebServlet(urlPatterns = { "/delete.do","/delete.html" })
public class delete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private IPersonneService personneService;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		personneService = ctx.getBean(IPersonneService.class);
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String idParam = request.getParameter("id");
		int id;
		String msg = "";
		ReponseStatut statut = ReponseStatut.OK;
		try {
			if (idParam == null || idParam.length() == 0) {
				statut = ReponseStatut.KO;
				msg = "L'id est obligatoire";
			} else {
				id = Integer.parseInt(idParam);

				if (this.personneService.deleteById(id)) {
					msg = "Suppression reussie avec succes";
				} else {
					statut = ReponseStatut.KO;
					msg = "Aucune personne ne poss�de cette id : "+id;
				}
			}
		} catch (Exception e) {
			statut = ReponseStatut.KO;
			msg = "L'id n'est pas un caract�rre num�rique ";
		}
		
		
		request.setAttribute("reponse", 
				ReponseDto.builder()
				.status(statut)
				.msg(msg).build());
		
		String nextJSP = "/list.do";
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
		dispatcher.forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
