package com.afpa.cda.servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.cda.dto.MetierDto;
import com.afpa.cda.dto.PersonneDto;
import com.afpa.cda.dto.ReponseStatut;
import com.afpa.cda.service.IMetierService;
import com.afpa.cda.service.IPersonneService;
import com.afpa.cda.tools.Utils;

@WebServlet(urlPatterns = { "/add.do", "/add.html" })
public class add extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private IPersonneService personneService;
	private IMetierService metierService;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		personneService = ctx.getBean(IPersonneService.class);
		metierService = ctx.getBean(IMetierService.class);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String nextJSP = "/jsp/add.jsp";
		request.setAttribute("listMetier", metierService.findAll());
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String metier = request.getParameter("metier");
		String adresse = request.getParameter("adresse");
		String date = request.getParameter("dateNaissance");
		
		String msg = "";
		ReponseStatut statut = ReponseStatut.OK;
		
		Integer idMetier = null;
		if(nom == null || nom.length() == 0
				|| prenom == null || prenom.length() == 0
				|| adresse == null || adresse.length() == 0
				|| metier == null || metier.length() == 0 
				|| date == null || date.length() == 0) {
			msg = "il manque des informations obligatoires";
			statut = ReponseStatut.KO;
		} else {
			try {
				idMetier = Integer.parseInt(metier);
				Optional<MetierDto> mDto = this.metierService.findById(idMetier);
				MetierDto mDtoo = new MetierDto();
				mDtoo.setId(idMetier);
				mDtoo.setLabel(mDto.get().getLabel());
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Integer id = this.personneService.ajouterUnePersonne(
						PersonneDto.builder()
						.nom(nom)
						.prenom(prenom)
						.metier(mDtoo)
						.adresse(adresse)
						.dateNaissance(sdf.parse(date))
						.build());
				msg = "personne ajoutée avec l'id "+id;
			} catch(NumberFormatException e) {
				msg = "valeur id metier non numérique "+metier;
				statut = ReponseStatut.KO;
			} catch (ParseException e) {
				msg = e.getMessage();
				statut = ReponseStatut.KO;
			}
		}
		Utils.forward(
				new HttpServletRequestWrapper(request) {
					@Override
					public String getMethod() {
						return "GET";
					}
				},
				response,
				getServletContext(),
				(statut==ReponseStatut.OK)?"/list.do":"/add.do",
						statut,	msg);
	}

}
