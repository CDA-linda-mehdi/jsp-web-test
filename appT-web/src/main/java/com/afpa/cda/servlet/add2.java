package com.afpa.cda.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.cda.dto.MetierDto;
import com.afpa.cda.dto.ReponseStatut;
import com.afpa.cda.service.IMetierService;
import com.afpa.cda.tools.Utils;

@WebServlet(urlPatterns = { "/add2.do", "/add2.html" })
public class add2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private IMetierService metierService;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		metierService = ctx.getBean(IMetierService.class);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String nextJSP = "/jsp/add2.jsp";
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String label = request.getParameter("label");

		String msg = "";
		ReponseStatut statut = ReponseStatut.OK;

		if (label == null || label.length() == 0) {
			msg = "il manque des informations obligatoires";
			statut = ReponseStatut.KO;
		} else {
			Integer id = this.metierService.ajouterUnMetier(MetierDto.builder().label(label).build());
			msg = "métier ajoutée avec l'id " + id;
		}
		Utils.forward(new HttpServletRequestWrapper(request) {
			@Override
			public String getMethod() {
				return "GET";
			}
		}, response, getServletContext(), (statut == ReponseStatut.OK) ? "/list.do" : "/add.do", statut, msg);
	}

}
