package com.afpa.cda.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.afpa.cda.dto.ReponseDto;
import com.afpa.cda.service.IPersonneService;
import com.afpa.cda.service.PersonneServiceImpl;

@WebServlet(urlPatterns = { "/index.html", "/list.do" })
public class list extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IPersonneService personneService;
	private int nbrPage = PersonneServiceImpl.nbrPage;
	private int fin = PersonneServiceImpl.fin;
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = getServletContext();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		personneService = ctx.getBean(IPersonneService.class);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nextJSP = "/jsp/index.jsp";
		String pageStr = request.getParameter("page");
		ReponseDto reponse = (ReponseDto) request.getAttribute("reponse");
		int page = 0;
		if (pageStr == null || pageStr.length() == 0) {

		} else {
			page = Integer.parseInt(pageStr);
		}		
		request.setAttribute("page", page);
		request.setAttribute("reponse", reponse);
		request.setAttribute("nbrPageI", nbrPage);
		request.setAttribute("finI", fin);
		request.setAttribute("personneService", personneService);
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
