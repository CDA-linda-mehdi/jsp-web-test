package com.afpa.cda.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.cda.entity.Metier;

@Repository
public interface MetierRepository extends CrudRepository<Metier, Integer> {
	@Query("SELECT m FROM Metier m")
	public List<Metier> findAll();
	@Query("SELECT m FROM Metier m WHERE m.label=:label")
	public Optional<Metier> findByLabel(@Param(value = "label") String label);
}
