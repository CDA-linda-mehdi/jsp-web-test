<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF8"
	pageEncoding="UTF-8"%>
<%@ page import="entity.Personne"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
<title>Projet JEE</title>
</head>
<body>
	<h1>Hello World</h1>
	<p>
		<%
			Personne personne = (Personne) request.getAttribute("personne1");
			//out.println("Bonjour " + personne.getNom() + " " + personne.getPrenom() + " " + personne.getAge());
		%>
	</p>

	<form action="affichage.do" method="get" readonly>
		<label for="nom">Nom</label> : <input type="text" name="nom"
			value="<%=personne.getNom()%>" /> <label for="prenom">Prénom</label>
		: <input type="text" name="prenom"
			value="<%out.append(personne.getPrenom());%>" /> <label for="age">Age</label>
		: <input type="number" name="age" value="<%=personne.getAge()%>" />
	</form>

	<br />

	<!-- Affichage de la liste de Personnes de manière aléatoire -->
	<table>
		<thead>
			<tr>
				<th>Nom</th>
				<th>Prénom</th>
				<th>Age</th>
			</tr>
		</thead>
		<tbody>

			<%
				List<Personne> list = (List<Personne>) request.getAttribute("listPers");
				int ligne = 0;
				for (Personne p : list) {
					if (ligne % 2 == 0) {
			%>

			<tr style="background-color: red">
				<td><%=p.getNom()%></td>
				<td><%=p.getPrenom()%></td>
				<td><%=p.getAge()%></td>
			</tr>
			<%
				} else {
			%>
			<tr style="background-color: green">
				<td><%=p.getNom()%></td>
				<td><%=p.getPrenom()%></td>
				<td><%=p.getAge()%></td>
			</tr>
			<%
				}
					ligne++;
				}
			%>





		</tbody>
	</table>

</body>
</html>